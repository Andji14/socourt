import { StackNavigator } from 'react-navigation';
import { StatusBar, Platform } from 'react-native';

import {
  MainScreen,
  SelectWhatYouPlay,
  SportType,
  StartGame,
  GameInProgress
} from '../screens';

export default StackNavigator({
  MainScreen: {
    screen: MainScreen,
  },
  StartGame: {
    screen: StartGame,
  },
  SelectWhatYouPlay: {
    screen: SelectWhatYouPlay,
  },
  SportType: {
    screen: SportType,
  },
  GameInProgress: {
    screen: GameInProgress,
  },
}, {
    headerMode: 'none',
    // cardStyle: { paddingTop: Platform.OS == 'ios' ? 15 : 0, backgroundColor: '#075394' },
  })



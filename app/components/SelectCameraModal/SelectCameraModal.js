
import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAwareScrollView,
    TextInput,
    Image
} from 'react-native';

import styles from './styles';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';



var radio_props = [
    { label: 'Camera 1', value: 0 },
    { label: 'Camera 2', value: 1 },
    { label: 'Camera 3', value: 2 },
    { label: 'All cameras', value: 3 }
];

class SelectCameraModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: 0,
        }
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                {/* <Image source={require('./image/back.png')} resizeMode="cover" style={styles.background} /> */}
                <Text style={styles.header}>Select cameras:</Text>
                <RadioForm
                    labelStyle={{ marginLeft: 18, fontSize: 20, color: '#d2da2c', fontFamily: 'Raleway-Bold' }}
                    radio_props={radio_props}
                    initial={0}
                    onPress={(value) => { this.setState({ value: value }) }}
                    style={{ alignItems: 'flex-start', marginVertical: 20 }}
                />

                <TouchableOpacity style={styles.buttonCont} onPress={this.props.onClose}>
                    <Text style={styles.buttonText}>OK</Text>
                </TouchableOpacity>

            </View>
        )
    }
}

export default SelectCameraModal






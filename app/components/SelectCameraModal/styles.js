import { StyleSheet } from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
  //$outline: 1,
  mainContainer: {
    marginTop: 100,
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 260,
    backgroundColor: 'black',
    borderColor: '#d2da2c',
    borderWidth: 2
  },
  background: {
    position: 'absolute',
    width: '80%',
    zIndex:20
},
  buttonCont: {
    width: '95%',
    height: 30,
    backgroundColor: '#d2da2c',
    justifyContent: 'center',
    alignItems: 'center',
    position:'absolute',
    bottom:4
  },
  buttonText: {
    color: 'black',
    fontSize:15,
    fontFamily: 'Raleway-Bold'
  },
  header:{
    color: '#d2da2c',
    fontSize:20,
    fontFamily: 'Raleway-Regular'
  },

})

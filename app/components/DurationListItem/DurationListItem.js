import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    Image,
    TouchableOpacity,
    TextInput
} from 'react-native';
import { StyleSheet } from 'react-native';
import styles from './styles';



class DurationListItem extends Component {

    render() {
        return (
            <TouchableOpacity style={styles.container}>
                <Text style={styles.text}>{this.props.duration}</Text>
                <Image source={this.props.picture} resizeMode="contain" style={styles.picture} />
            </TouchableOpacity>
        );
    }
}
export default DurationListItem

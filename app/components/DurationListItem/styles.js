import { StyleSheet } from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
  //$outline: 1,
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 50
  },
  text:{
    color:'white',
    fontSize:17,
    fontFamily:'Raleway-Regular',
  },
  picture:{
    width:25
  },
});
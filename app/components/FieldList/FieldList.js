
import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    Image,
    TouchableOpacity,
    FlatList
} from 'react-native';
import { StyleSheet } from 'react-native';
import styles from './styles';
import { FieldListItem } from '../FieldListItem';


export default class FieldList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [
                {
                    key: 1,
                    fieldNimber: '1'
                },
                {
                    key: 2,
                    fieldNimber: '2'
                },
                {
                    key: 3,
                    fieldNimber: '3'
                },
                {
                    key: 4,
                    fieldNimber: '4'
                },
                {
                    key: 5,
                    fieldNimber: '5'
                },
                {
                    key: 6,
                    fieldNimber: '6'
                },
            ]
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    keyExtractor={item => item}
                    horizontal={true}
                    data={this.state.users}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item }) => (
                        <FieldListItem
                            onPress={() => { console.log('test'); }}
                            fieldNimber={item.fieldNimber}
                        />
                    )}
                />

            </View>
        );
    }
}







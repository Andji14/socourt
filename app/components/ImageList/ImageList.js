
import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    Image,
    TouchableOpacity,
    FlatList
} from 'react-native';
import { StyleSheet } from 'react-native';
import styles from './styles';
import { ImageItem } from '../ImageItem';



export default class ImageList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [
                {
                    key: 1,
                    image: require('./images/clip.png'),
                    text:'CAM 1'
                    
                },
                {
                    key: 2,
                    image: require('./images/clip.png'),
                    text:'CAM 2'
                },
                {
                    key: 3,
                    image: require('./images/clip.png'),
                    text:'CAM 3'
                },
                {
                    key: 4,
                    image: require('./images/clip.png'),
                    text:'CAM 4'
                },
                {
                    key: 5,
                    image: require('./images/clip.png'),
                    text:'CAM 5'
                },

            ]
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    horizontal={true}
                    keyExtractor={item => item.key}
                    data={this.state.users}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) => (
                        <ImageItem
                            image={item.image}
                            text={item.text}
                            
                        />

                    )}
                />

            </View>
        );
    }
}

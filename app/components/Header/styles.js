import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
    //$outline: 1,
    headerContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 110,
        width: width,
    },
    logo: {
        height: 40,
        width: 250,
    },
    closeContainer: {
        position: 'absolute',
        right: 15,
        top: 44
    },
    close: {
        width: 20,
        height: 20,
    },

});
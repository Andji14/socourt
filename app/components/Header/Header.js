import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';

import styles from './styles';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../../config.json';
const Icon = createIconSetFromFontello(fontelloConfig);

class Header extends Component {
    render() {
        return (
            <View style={styles.headerContainer}>
                <Image source={require('./images/startgame.png')} resizeMode="contain" style={styles.logo} />
                <TouchableOpacity style={styles.closeContainer} >
                    <Icon name="cross" size={22} color="white" />
                </TouchableOpacity>
            </View>
        )
    }
}
export default Header

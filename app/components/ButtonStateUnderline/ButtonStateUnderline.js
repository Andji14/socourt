import React, { Component } from 'react';

import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image
} from 'react-native'

import styles from './styles';

export default class ButtonStateUnderline extends Component {
  constructor(props) {
    super(props)
    this.state = {
      checklistItems: [
        {
          text: 'SomeText',
          selected: true,
          state: 0,
        },
        {
          text: 'SomeText',
          selected: false,
          state: 0,
        },
        {
          text: 'SomeText',
          selected: false,
          state: 0,
        },
      ]
    };
  }

  render() {
    let iconColor = '#acb1b5';
    if(this.props.state != 0){
      iconColor = '#00a0e3';
    }
    let commentsColor = [styles.circleCont2];
    let commentIconColor = '#ffffff'
    let stateOneColorStyles = [styles.circleCont2];
    let stateTwoColorStyles = [styles.circleCont2];
    let stateThreeColorStyles = [styles.circleCont2];
    if(this.props.comments > 0){
      commentsColor.push({
        backgroundColor: '#ffffff'
      })
      commentIconColor = '#58626b'
    }
    if(this.props.state == 1){
      stateOneColorStyles.push({
        backgroundColor: '#ed0809',
      });
    }
    if(this.props.state == 2){
      stateTwoColorStyles.push({
        backgroundColor: '#ebec07',
      });
    }
    if(this.props.state == 3){
      stateThreeColorStyles.push({
        backgroundColor: '#0bf20d',
      });
    }
    return (

      <View style={styles.keyNumCont2}>

        <TouchableWithoutFeedback onPress={() => {
          if (this.props.changeState) {
            if (this.props.state == 1) {
              this.props.changeState.call(null, 0)
            } else {
              this.props.changeState.call(null, 1)
            }
          }
        }}>
          <View style={[styles.circleCont2, stateOneColorStyles]}>
            <Icon name="missing" size={20} color="#acb1b5" style={styles.headerIcon} />
          </View>
        </TouchableWithoutFeedback>





        <TouchableWithoutFeedback onPress={() => {
          if (this.props.changeState) {
            if (this.props.state == 2) {
              this.props.changeState.call(null, 0)
            } else {
              this.props.changeState.call(null, 2)
            }
          }
        }}>
          <View style={[styles.circleCont2, stateTwoColorStyles]}>
            <Icon name="zabelejka" size={20} color="#acb1b5" style={styles.headerIcon} />
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={() => {
          if (this.props.changeState) {
            if (this.props.state == 3) {
              this.props.changeState.call(null, 0)
            } else {
              this.props.changeState.call(null, 3)
            }
          }
        }}>
          <View style={[styles.circleCont2, stateThreeColorStyles]}>
            <Icon name="ok" size={20} color="#acb1b5" style={styles.headerIcon} />
          </View>
        </TouchableWithoutFeedback>

      </View>






      // <View style={styles.buttonLineContainer2} >
      //   <TouchableOpacity style={styles.buttons2} onPress={this.myFunctions.bind(this)}>
      //     <Text style={this.props.selected ? styles.buttonText : styles.buttonText2}>{this.props.textButton}</Text>
      //   </TouchableOpacity>





      //   {this.state.pinter && (
      //     <Image source={require('./images/pointer.png')} resizeMode="contain" style={styles.pointer2} />
      //   )}
      //   {this.state.simpleline && (
      //     <Image source={require('./images/simpleline.png')} resizeMode="contain" style={styles.pointer2} />
      //   )}

      // </View>



    );
  }
}

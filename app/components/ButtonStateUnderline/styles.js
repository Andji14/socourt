import { StyleSheet } from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
  //$outline: 1,
//   coloredButton: {
//     width: 80,
//     height: 22,
//     borderRadius: 15,
//     justifyContent: 'center'
// },

buttons2: {
  width: '100%',
  height: 40,
  borderRadius: 5,
  borderColor: 'white',
  borderWidth: 1,
  alignItems: 'center',
  justifyContent: 'center',

},
buttonText2: {
  color: 'white',
  fontSize: 15,
  fontFamily: 'Raleway-Regular'
},
buttonText: {
  color: 'red',
  fontSize: 15,
  fontFamily: 'Raleway-Regular'
},
pointer2: {

  width: '100%'
},
buttonLineContainer2:{
  width: (width / 3) - 20,
  alignItems: 'center',
  justifyContent: 'center',
  height: 60,
},
buttonContainer:{
  flexDirection:'row',
  width:width,
  alignItems: 'center',
  justifyContent: 'space-around',
},

///////////////////////////////////
keyNumContText: {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'flex-start',
  margin: 10,
  marginLeft: 20
},
text1: {
  color: 'white',
  fontSize: 16,
  marginLeft: 10,
  borderBottomWidth: 1,
  borderBottomColor: 'white',
  backgroundColor: 'transparent',
},
circleCont: {
  width: 30,
  height: 30,
  borderWidth: 1,
  borderRadius: 15,
  borderColor: 'white',
  justifyContent: 'center',
  alignItems: 'center',
  marginHorizontal: 5
},
headerText: {
  color: 'white',
  fontWeight: 'bold',
  fontSize: 20,
  textAlign: 'center',
  backgroundColor: 'transparent',
},
textCont: {
  width: 50,
  height: 30,
  borderRadius: 10,
  backgroundColor: 'white',
  justifyContent: 'center',
  alignItems: 'center',
},
headerText1: {
  color: '#075394',
  fontWeight: 'bold',
  fontSize: 20,
  backgroundColor: 'transparent',
},
keyNumCont2: {
  flexDirection: 'row',
  justifyContent: 'space-around',
  alignItems: 'flex-start',
},
circleCont2: {
  width: 65,
  height: 28,
  borderRadius: 20,
  backgroundColor: '#58626b',
  borderColor: '#00a0e3',
  borderWidth: 0.5,
  justifyContent: 'center',
  alignItems: 'center',
},
dashCont: {
  justifyContent: 'flex-start'
},
dash: {
  justifyContent: 'flex-start',
  color: 'white',
  fontSize: 25,
  backgroundColor: 'transparent',
},
comment: {
  top: -3,
  color: 'white',
  backgroundColor: 'transparent',
},
});
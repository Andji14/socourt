#Horizontal users list

##Should have:
* avatar
* username
* userDescription
* onPress of user component
---
##Example usage:

```javascript
  import { HorizontalUserList } from '../components';
  ...

  render(){
    return (
        <HorizontalUserList/>
    );
  }
```

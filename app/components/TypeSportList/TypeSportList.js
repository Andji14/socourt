
import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    Image,
    TouchableOpacity,
    FlatList
} from 'react-native';
import { StyleSheet } from 'react-native';
import styles from './styles';
import { TypeSportListItem } from '../TypeSportListItem';


export default class TypeSportList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [
                {
                    key: 1,
                    userImage: require('./img/table_tennis.png'),
                    userName: 'Table', 
                    userDescription: 'Tennis'
                },
                {
                    key: 2,
                    userImage: require('./img/handball.png'),
                    userName: 'Handball',
                    // userDescription: 'Lorem Ipsum'
                },
                {
                    key: 3,
                    userImage: require('./img/squash.png'),
                    userName: 'Squash',
                    // userDescription: 'Lorem Ipsum'
                },
               
            ]
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    keyExtractor={item => item}
                    horizontal={true}
                    data={this.state.users}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item }) => (
                        <TypeSportListItem
                            onPress={() => { console.log('test'); }}
                            userImage={item.userImage}
                            userName={item.userName}
                            userDescription={item.userDescription}
                        />
                    )}
                />

            </View>
        );
    }
}







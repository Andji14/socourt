import { StyleSheet } from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
  //$outline: 1,
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 120
  },
  imageContainer: {
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 5,
    borderColor:'white',
    borderWidth:2

  },
  userImage: {
    width: 48,
  },
  text: {
    color: 'white',
    textAlign: 'center',
    fontSize:15,
    fontFamily:'Raleway-SemiBold'
  },
  textContainer: {
    marginLeft:5,
    justifyContent: 'center',
    alignItems: 'flex-start',
  }

});
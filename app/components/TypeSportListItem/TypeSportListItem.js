import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    Image,
    TouchableOpacity,
    TextInput
} from 'react-native';
import { StyleSheet } from 'react-native';
import styles from './styles';



class TypeSportListItem extends Component {

    render() {
        return (
            <TouchableOpacity  style={styles.container}>
                <View style={styles.imageContainer} >
                    <Image source={this.props.userImage} resizeMode="contain" style={styles.userImage} />
                </View>
                <View style={styles.textContainer} >
                    <Text style={styles.text}>{this.props.userName}</Text>
                    <Text style={styles.text}>{this.props.userDescription}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}
export default TypeSportListItem

#Single User list item

##Should have:
* avatar
* username

---
##Example usage:

```javascript
  import { SingleUserListItem } from '../components';
  ...

  render(){
    return (
        <SingleUserListItem
          userImage={require('./img/AvatarGirl.png')}
          userName='Jimmy Choo'
        />
    );
  }
```

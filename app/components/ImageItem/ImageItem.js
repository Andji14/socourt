import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    Image,
    TouchableOpacity,
    TextInput
} from 'react-native';
import { StyleSheet } from 'react-native';
import styles from './styles';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../../config.json';
const Icon = createIconSetFromFontello(fontelloConfig);


class ImageItem extends Component {

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.imageContainer}>
                    <Image source={this.props.image} resizeMode="contain" style={styles.image} />
                    <TouchableOpacity style={styles.playImageCont}>
                        <Image source={require('./image/play.png')} resizeMode="contain" style={styles.playImage} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.garbage}>
                        <Icon name="dustbin-thin" size={25} color="white"  />
                    </TouchableOpacity >
                    <TouchableOpacity style={styles.garbageCont} />
                </View>
                <Text style={styles.camText}>{this.props.text}</Text>
            </View>
        );
    }
}
export default ImageItem
import { StyleSheet } from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
    //$outline: 1,
    container: {
        height: 280,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 20,
    },
    imageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        borderColor: '#d1da29',
        borderWidth: 2,
        height: 250,
    },
    playImageCont: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center'
    },
    playImage: {
        width: 55,
    },
    garbageCont: {
        position: 'absolute',
        bottom: 10,
        right: 10,
        justifyContent: 'center',
        alignItems: 'center',
        width: 45,
        height: 45,
        borderRadius: 23,
        backgroundColor: '#2b3c4e',
        opacity: 0.3
    },
    garbage: {
        zIndex: 50,
        position: 'absolute',
        bottom: 20,
        right: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    camText: {
        fontSize: 15,
        fontFamily: 'Raleway-SemiBold',
        color: 'white',
        alignSelf: 'flex-start',
        marginLeft: 20,
        marginVertical: 5

    },

});
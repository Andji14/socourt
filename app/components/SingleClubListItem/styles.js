import { StyleSheet } from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
  //$outline: 1,
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 80,
    height: 90,
  
  },
  imageContainer: {
    width:40,
    height:40,
    borderRadius:20,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical:5
  },
  userImage: {
    width: 40,
    marginHorizontal: 5,
    borderRadius: 20
  },
  text: {
    color: 'white',
    textAlign: 'center',
    lineHeight:15,
    fontFamily:'Raleway-SemiBold',
    fontSize:13
  },
  textContainer: {
    justifyContent: 'center',
    alignItems: 'center',

  }

});
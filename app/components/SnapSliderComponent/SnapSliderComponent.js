import React, { Component } from 'react';
import SnapSlider from 'react-native-snap-slider';
import styles from './styles';

import {
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class SnapSliderComponent extends Component {
  sliderOptions = [
      {value: 0, label: '20'},
      {value: 1, label: '40'},
      {value: 2, label: '60'},
      {value: 3, label: '90'},
      {value: 4, label: '120'},
      {value: 5, label: require('./img/inf.png')},

  ];

  constructor(props) {
    super(props);
    this.state = this.getInitialState();
    this.slidingComplete = this.slidingComplete.bind(this);
  }

  getInitialState() {
      return {
          defaultItem: 2
      };
  }

  slidingComplete(itemSelected) {
      console.log("slidingComplete");
      console.log("item selected " + this.refs.slider.state.item);
      console.log("item selected(from callback)" + itemSelected);
      console.log("value " + this.sliderOptions[this.refs.slider.state.item].value);
  }

  render() {
    return (
        <View style={styles.container}>
            <Text style={styles.welcome}>
                SnapSlider Example!
            </Text>
            <SnapSlider ref="slider" containerStyle={styles.snapsliderContainer} style={styles.snapslider}
                itemWrapperStyle={styles.snapsliderItemWrapper}
                itemStyle={styles.snapsliderItem}
                items={this.sliderOptions}
                labelPosition="bottom"
                defaultItem={this.state.defaultItem}
                onSlidingComplete={this.slidingComplete} />
        </View>
    );
  }
}




import SnapSliderComponent from './SnapSliderComponent';
import Styles from './styles';

export {
    SnapSliderComponent,
    Styles
}
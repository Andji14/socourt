import { StyleSheet } from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
    //$outline: 1,
    container: {
        width:width-10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    },

    snapsliderContainer: {
        borderWidth: 0,
        backgroundColor: 'transparent',
        // backgroundColor:'pink'
    },
    snapslider: {
        width:width,
        borderWidth: 3,
        borderColor:'red',
        backgroundColor:'pink',
        // color:'red'
    },
    snapsliderItemWrapper: {
        borderWidth: 0,
        marginHorizontal:8
        // fontFamily:'Raleway-Bold'
    },
    snapsliderItem: {
        borderWidth: 0,
        borderColor:'red',
        fontFamily:'Raleway-Bold',
    }
});
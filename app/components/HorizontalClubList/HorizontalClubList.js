
import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    Image,
    TouchableOpacity,
    FlatList
} from 'react-native';
import { StyleSheet } from 'react-native';
import styles from './styles';
import { SingleClubListItem } from '../SingleClubListItem';


export default class HorizontalClubList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [
                {
                    key: 1,
                    userImage: require('./img/avatar.png'),
                    userName: 'Maleeva',
                    userDescription: 'Club'

                },
                {
                    key: 2,
                    userImage: require('./img/avatar.png'),
                    userName: 'Maleeva',
                    userDescription: 'Club'
                },
                {
                    key: 3,
                    userImage: require('./img/avatar.png'),
                    userName: 'Maleeva',
                    userDescription: 'Club'
                },
                {
                    key: 4,
                    userImage: require('./img/avatar.png'),
                    userName: 'Maleeva',
                    userDescription: 'Club'
                },
                {
                    key: 5,
                    userImage: require('./img/avatar.png'),
                    userName: 'Maleeva Club',
                },
                {
                    key: 6,
                    userImage: require('./img/avatar.png'),
                    userName: 'Maleeva',
                    userDescription: 'Club'
                },
                {
                    key: 7,
                    userImage: require('./img/avatar.png'),
                    userName: 'Maleeva',
                    userDescription: 'Club'
                },

            ]
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    keyExtractor={item => item}
                    horizontal={true}
                    data={this.state.users}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item }) => (
                        <SingleClubListItem
                            onPress={this.props.toggleDiv}
                            userImage={item.userImage}
                            userName={item.userName}
                            userDescription={item.userDescription}
                        />
                    )}
                />

            </View>
        );
    }
}







import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    Image,
    TouchableOpacity,
    TextInput
} from 'react-native';
import { StyleSheet } from 'react-native';
import styles from './styles';



class FieldListItem extends Component {

    render() {
        return (
            <TouchableOpacity  style={styles.container}>
                <View style={styles.textContainer} >
                    <Text style={styles.text}>{this.props.fieldNimber}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}
export default FieldListItem

import { StyleSheet } from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
  //$outline: 1,
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 60
  },
  textContainer: {
    width:50,
    height:50,
    borderRadius:25,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor:'white',
    borderWidth:1
  },
  text:{
    color:'white',
    fontSize:18,
    fontFamily:'Raleway-Regular',
  },

});
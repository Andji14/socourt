
import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    Image,
    TouchableOpacity,
    FlatList
} from 'react-native';
import { StyleSheet } from 'react-native';
import styles from './styles';
import { DurationListItem } from '../DurationListItem';


export default class DurationList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [
                {
                    key: 1,
                    duration: '20'
                },
                {
                    key: 2,
                    duration: '40'
                },
                {
                    key: 3,
                    duration: '60'
                },
                {
                    key: 4,
                    duration: '90'
                },
                {
                    key: 5,
                    duration: '120'
                },
                {
                    key: 6,
                    duration: '150'
                },
                {
                    key: 7,
                    picture: require('./img/ss.png'),
                },

            ]
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Image source={require('./img/line.png')} resizeMode="contain" style={styles.line} />
                    <Image source={require('./img/logo-balls-only.png')} resizeMode="contain" style={styles.logo} />
                </View>
                <FlatList
                    keyExtractor={item => item}
                    horizontal={true}
                    data={this.state.users}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item }) => (
                        <DurationListItem
                            onPress={() => { console.log('test'); }}
                            duration={item.duration}
                            picture={item.picture}
                        />
                    )}
                />

            </View>
        );
    }
}







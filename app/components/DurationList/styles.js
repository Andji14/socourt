import { StyleSheet } from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
  //$outline: 1,
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  line: {
    width: width - 20
  },
  logo: {
    width: 60,
    position:'absolute',
    top:-5,
    left:100
  },


});
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Image
} from 'react-native';

import styles from './styles';

class Options extends Component {
    render() {
        return (
            <View style={styles.sportContainer}>
                <View style={styles.threePartsContainer}>
                    <View style={styles.checkContainer}>
                        <Image source={this.props.imageLeft} resizeMode="contain" style={styles.success} />
                    </View>
                    <View style={styles.middleContainer}>
                        <Image source={this.props.imageCenter} resizeMode="contain" style={styles.sport} />
                    </View>
                    <View style={styles.containerMinutes}>
                        {this.props.image && (
                            <View style={styles.sportImageContainer}>
                                <Image source={this.props.imageRight} resizeMode="contain" style={styles.typeSport} />
                            </View>

                        )}
                        {this.props.blankCircle && (
                            <View style={styles.imageMinutes} >
                                <Text style={styles.textMinutes}>{this.props.minutes}</Text>
                            </View>
                        )}
                        {this.props.chooseFieldNumber && (
                            <View style={styles.fieldNumCont}>
                                <Text style={styles.minutes}>{this.props.fieldNumber}</Text>
                            </View>
                        )}
                        <View style={styles.textContainer} >
                            <Text style={styles.Minutes}>{this.props.text}</Text>
                            <Text style={styles.Minutes}>{this.props.rightTextTop}</Text>
                            <Text style={styles.Minutes}>{this.props.rightTextBottom}</Text>
                        </View>
                    </View>
                </View>
                <View>
                    <Text style={styles.selectText}>{this.props.description}</Text>
                </View>
            </View>
        )
    }
}
export default Options

##Example usage:

```javascript
  import { Options } from '../components';
  ...

  render(){
    return (
           <Options
            imageLeft={require('./images/success.png')}
            imageCenter={require('./images/sport-center-icon.png')}
            imageRight={require('./images/squash.png')}
            blankCircle
            rightTextTop='Maleeva'
            rightTextBottom='Club'
            description='Select where you play:'
          />
    );
  }
```
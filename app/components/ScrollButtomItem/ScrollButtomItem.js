import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    Image,
    TouchableOpacity,
    TextInput
} from 'react-native';
import { StyleSheet } from 'react-native';
import styles from './styles';



class ScrollButtomItem extends Component {

    render() {
        return (

            <View style={styles.mainContainer}  onPress={() => null}>
                <View style={styles.topContainer}>
                    <TouchableOpacity style={styles.bigContainer}>
                        <Image source={this.props.image} resizeMode="contain" style={styles.image} />
                    </TouchableOpacity>
                    <View style={styles.smallContainer}>
                        <Text style={styles.or}>{this.props.textBetween}</Text>
                    </View>
                </View>


                <View style={styles.bottomContainer}>
                    <View style={styles.textContainer}>
                        <Text style={styles.header}>{this.props.header}</Text>
                        <Text style={styles.text}>{this.props.text}</Text>
                    </View>
                    <View style={styles.smallContainer}>
                        <Image source={this.props.smallImage}  resizeMode="contain"  style={styles.spread} />
                    </View>
                </View>
            </View>


        );
    }
}
export default ScrollButtomItem
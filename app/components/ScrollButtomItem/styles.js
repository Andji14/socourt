import { StyleSheet } from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
    //$outline: 1,
    mainContainer: {
        width: 200,
        height: 160,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image:{
        height:80,
        // backgroundColor:'red'
    },



////        TOP
    topContainer: {
        height: 80,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    bigContainer: {
        height: 100,
        width: 200,
        // backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    smallContainer: {
        display: 'flex',
        width: 40,
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
    },


////        BOTTOM
    bottomContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textContainer: {
        width: 200,
        // backgroundColor: 'blue',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    header: {
        color: 'white',
        fontSize: 13,
        textAlign: 'center',
        fontFamily:'Raleway-Regular' ,
        marginVertical:5
    },
    text: {
        color: 'white',
        fontSize: 12,
        textAlign: 'center',
        fontFamily:'Raleway-Regular' 
    },
    spread: {
        height: 30
    },
    or: {
        color: 'white',
        fontSize: 18,
        fontFamily:'Raleway-Bold'
    },

});
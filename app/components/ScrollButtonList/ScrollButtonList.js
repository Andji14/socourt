
import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    Image,
    TouchableOpacity,
    FlatList
} from 'react-native';
import { StyleSheet } from 'react-native';
import styles from './styles';
import { ScrollButtomItem } from '../ScrollButtomItem';



export default class ScrollButtonList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [
                {
                    key: 1,
                    image: require('./images/bracelet.png'),
                    textBetween: 'OR',
                    header: 'USE YOUR WRISTBAND',
                    text: '(video from all cameras)',
                },
                {
                    key: 2,
                    image: require('./images/software_bttn.png'),
                    header: 'USE THIS BUTTON HERE',
                    text: '(video from all cameras)',
                    smallImage: require('./images/spread.png'),
                },
                {
                    key: 3,
                    image: require('./images/software_bttn.png'),
                    header: 'USE THIS BUTTON HERE',
                    text: '(video from all cameras 1&4)',
                    smallImage: require('./images/spread.png'),
                },
                {
                    key: 4,
                    image: require('./images/software_bttn.png'),
                    header: 'USE THIS BUTTON HERE',
                    text: '(video from all camera 3 )',
                    smallImage: require('./images/spread.png'),
                },
                {
                    key: 5,
                    image: require('./images/software_bttn.png'),
                    header: 'USE THIS BUTTON HERE',
                    text: '(video from all camera 4 )',
                    smallImage: require('./images/spread.png'),
                },

            ]
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    horizontal={true}
                    keyExtractor={item => item.key}
                    data={this.state.users}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) => (
                        <ScrollButtomItem
                            image={item.image}
                            header={item.header}
                            text={item.text}
                            textBetween={item.textBetween}
                            smallImage={item.smallImage}
                        />

                    )}
                />

            </View>
        );
    }
}

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  HorizontalClubList,
  TypeSportList,
  FieldList,
  DurationList,
  Options,
  Header,
  ButtonStateUnderline
} from '../../components';

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image
} from 'react-native';

import styles from './styles';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../../config.json';
const Icon = createIconSetFromFontello(fontelloConfig);

class MainScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ButtonSelected: 'Around you',
    }
  }
  render() {
    return (
      <View style={styles.wrapper}>
        <Image source={require('./images/background.png')} style={styles.background} />

        <Header />

        <Options
          imageCenter={require('./images/sport-center-icon.png')}
          description='Select where you play:'
        />

        <View style={styles.selectContainer}>
          <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.buttons}>
              <Text style={[styles.buttonText, { fontFamily: 'Raleway-SemiBold' }]}>Around you </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttons}>
              <Text style={styles.buttonText}>Search</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttons}>
              <Text style={styles.buttonText}>All</Text>
            </TouchableOpacity>
          </View>
          <Image source={require('./images/pointer-active.png')} resizeMode="contain" style={styles.pointer} />
          < HorizontalClubList />
        </View>
        <TouchableOpacity onPress={() => { this.props.navigation.navigate('SelectWhatYouPlay') }} style={styles.footerContainer}>
          <Text style={styles.footerText}>START GAME</Text>
        </TouchableOpacity>

      </View>
    )
  }
}

export default MainScreen

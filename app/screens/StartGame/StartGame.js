import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  HorizontalClubList,
  TypeSportList,
  FieldList,
  DurationList,
  Options,
  Header,
  SnapSliderComponent
} from '../../components';

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image
} from 'react-native';

import styles from './styles';

class StartGame extends Component {
  render() {
    return (
      <View style={styles.wrapper}>
        <Image source={require('./images/background.png')} style={styles.background} />
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.container}>
          <Header />
          <Options
            imageLeft={require('./images/success.png')}
            imageCenter={require('./images/sport-center-icon.png')}
            blankCircle
            rightTextTop='Maleeva'
            rightTextBottom='Club'
            description='Sport Center'
          />
          <Options
            imageLeft={require('./images/success.png')}
            imageCenter={require('./images/merged3icons.png')}
            imageRight={require('./images/squash.png')}
            image
            text='Squash'
            description='Sport Type'
          />
          <Options
            imageLeft={require('./images/success.png')}
            imageCenter={require('./images/pitch.png')}
            chooseFieldNumber
            fieldNumber='2'
            description='Field Number'
          />
          <Options
            imageCenter={require('./images/timer.png')}
            blankCircle
            text='Minutes'
            minutes='120'
            description='Set game duration:'
          />

          <View style={styles.durationContainer}>
            <DurationList />
          </View>

          <TouchableOpacity style={styles.footerContainer}>
            <Text style={styles.footerText}>START GAME</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    )
  }
}

export default StartGame

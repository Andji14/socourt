import { StyleSheet } from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
    //$outline: 1,
    wrapper: {

        width: width,
        height: height,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    container: {
        display: 'flex',
        width: width,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    background: {
        position: 'absolute',
        width: width,
        height: height,
        //opacity: 0.6
    },
    headerContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 110,
        width: width,
    },
    logo: {
        height: 40,
        width: 250,
    },
    sport: {
        height: 35,
    },
    typeSport:{
        width:45
    },
    closeContainer: {
        position: 'absolute',
        right: 15,
        top: 45
    },
    close: {
        width: 20,
        height: 20,
    },
    sportContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:15
    },
    selectText: {
        color: 'white',
        fontSize: 18,
        fontFamily: 'Raleway-Regular',
        top:-6
    },
    buttonContainer: {
        flexDirection: 'row',
        width: '100%',
        height: 40,
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    buttons: {
        width: (width / 3) - 20,
        height: 40,
        borderRadius: 5,
        borderColor: 'white',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        color: 'white',
        fontSize: 15,
        fontFamily: 'Raleway-Regular'
    },
    footerContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
        height: 70,
        backgroundColor: '#00cbff',
        marginBottom: 25
    },
    footerText: {
        color: 'black',
        fontSize: 25,
        fontFamily: 'Intro_Inline'
    },
    containerMinutes: {
        flex: 3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 80
    },
    imageMinutes: {
        width: 45,
        height: 45,
        borderRadius: 22.5,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 5,
        backgroundColor: '#d1da29'
    },
    sportImageContainer:{
        width: 45,
        height: 45,
        borderRadius: 22.5,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 5,
    },
    fieldNumCont:{
        width: 45,
        height: 45,
        borderRadius: 22.5,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 5,
        borderColor:'#d1da29',
        borderWidth:1
    },
    textMinutes: {
        color: 'black',
        textAlign: 'center',
        fontSize: 18,
        fontFamily: 'Raleway-Bold'
    },
    Minutes: {
        color: 'white',
        textAlign: 'center',
        fontSize: 15,
        fontFamily: 'Raleway-Regular'
    },
    minutes:{
        color: 'white',
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'Raleway-Regular' 
    },
    textContainer: {
        marginLeft: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    threePartsContainer: {
        flexDirection: 'row',
        width: width - 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    checkContainer: {
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    middleContainer: {
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    success: {
        width: 30
    },
    pointer: {
        width: width - 20
    },
    selectContainer: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical:20,
    },
    durationContainer:{
        marginVertical:20
    },

});
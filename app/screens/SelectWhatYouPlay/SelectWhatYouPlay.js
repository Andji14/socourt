import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  HorizontalClubList,
  TypeSportList,
  FieldList,
  DurationList,
  Options,
  Header
} from '../../components';

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image
} from 'react-native';

import styles from './styles';

class SelectWhatYouPlay extends Component {
  render() {
    return (
      <View style={styles.wrapper}>
        <Image source={require('./images/background.png')} style={styles.background} />
        
        <Header />
        <Options
          imageLeft={require('./images/success.png')}
          imageCenter={require('./images/sport-center-icon.png')}
          blankCircle
          rightTextTop='Maleeva'
          rightTextBottom='Club'
          description='Sport Center'
        />
        <Options
          imageCenter={require('./images/merged3icons.png')}
          description='Select what you play:'
        />
        <View style={{ marginTop: 10 }}>
          <TypeSportList />
        </View>

        <TouchableOpacity onPress={() => { this.props.navigation.navigate('SportType') }} style={styles.footerContainer}>
          <Text style={styles.footerText}>START GAME</Text>
        </TouchableOpacity>

      </View>
    )
  }
}
export default SelectWhatYouPlay

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  HorizontalClubList,
  TypeSportList,
  FieldList,
  DurationList,
  Options,
  Header
} from '../../components';

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image
} from 'react-native';

import styles from './styles';

class SportType extends Component {
  render() {
    return (
      <View style={styles.wrapper}>
        <Image source={require('./images/background.png')} style={styles.background} />

        < Header />

        <Options
          imageLeft={require('./images/success.png')}
          imageCenter={require('./images/sport-center-icon.png')}
          blankCircle
          rightTextTop='Maleeva'
          rightTextBottom='Club'
          description='Sport Center'
        />
        <Options
          imageLeft={require('./images/success.png')}
          imageCenter={require('./images/merged3icons.png')}
          imageRight={require('./images/squash.png')}
          image
          text='Squash'
          description='Sport Type'
        />


        <View style={styles.selectContainer}>
          <Options
            imageCenter={require('./images/pitch.png')}
            description='Select field number:'
          />
          < FieldList />
        </View>

        <TouchableOpacity onPress={() => { this.props.navigation.navigate('StartGame') }} style={styles.footerContainer}>
          <Text style={styles.footerText}>START GAME</Text>
        </TouchableOpacity>

      </View>
    )
  }
}

export default SportType

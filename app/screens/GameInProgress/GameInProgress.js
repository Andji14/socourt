import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  ScrollButtonList,
  ImageList,
  SelectCameraModal
} from '../../components';

import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Modal
} from 'react-native';

import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../../config.json';
const Icon = createIconSetFromFontello(fontelloConfig);
import styles from './styles';

class GameInProgress extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectCameraModal: false,
    }
  }
  render() {
    return (
      <View style={styles.wrapper}>
        <Image source={require('./images/back.png')} resizeMode="cover" style={styles.background} />
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.container}>
          <Text style={styles.header}>Tap AFTER a good moment! </Text>

          <ScrollButtonList />

          <View style={styles.gameInProgressContainer}>
            <View style={styles.circle}></View>
            <View style={styles.textCont}>
              <Text style={styles.sysText}>SYSTEM </Text>
              <Text style={styles.sysText}>OK</Text>
            </View>
            <TouchableOpacity onPress={() => { this.setState({ selectCameraModal: true }) }}>
              <Text style={styles.gameHeader}>GAME IN PROGRESS</Text>
            </TouchableOpacity>
            <Image source={require('./images/clock.png')} resizeMode="contain" style={styles.clock} />
            <Text style={styles.timeText}>00:14 </Text>
          </View>

          <View style={styles.timeHeader}>
            <Text style={styles.clockHeader}>21:30H</Text>
            <TouchableOpacity  style={styles.icon} >
              <Icon name="dustbin-thin" size={25} color="white"/>
            </TouchableOpacity>
          </View>
          < ImageList />
          <View style={styles.timeHeader}>
            <Text style={styles.clockHeader}>21:45H</Text>
            <TouchableOpacity style={styles.icon} >
              <Icon name="dustbin-thin" size={25} color="white" />
            </TouchableOpacity>
          </View>
          < ImageList />



          <TouchableOpacity style={styles.footerContainer}>
            <Text style={styles.footerText}>END GAME</Text>
          </TouchableOpacity>


          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.selectCameraModal}
            onRequestClose={() => { alert("Modal has been closed.") }}
          >
            <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center' }}>
              <SelectCameraModal  onClose={() => { this.setState({ selectCameraModal: false }) }} />
            </View>
          </Modal>
        </ScrollView>
      </View>
    )
  }
}

export default GameInProgress

import { StyleSheet } from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
    //$outline: 1,
    background: {
        position: 'absolute',
        width: '100%',
    },
    wrapper: {
        width: width,
        height: height,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    container: {
        display: 'flex',
        width: width,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    header: {
        marginTop: 20,
        fontFamily: 'Raleway-SemiBold',
        fontSize: 20,
        color: 'white'
    },
    gameInProgressContainer: {
        width:width,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginTop:10
    },
    circle: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: '#d1da29',
    },
    test:{
        fontSize: 10, 
    },
    clock:{
        width:25
    },
    textCont:{
        justifyContent: 'center',
        alignItems: 'center',
            },
    gameHeader:{
        fontSize: 20,
        borderBottomWidth:1,
        borderBottomColor:'#00cbff',
        color:'#00cbff',
        fontFamily:'Intro'
    },
    timeText:{
        fontSize: 18,
        borderBottomWidth:1,
        borderBottomColor:'white',
        color:'white',
        fontFamily:'Intro',
        fontStyle:'italic'
    },
    sysText:{
        color:'#d1da29',
        fontFamily:'Intro',
        lineHeight:15
    },
    timeHeader:{
        marginTop:30,
        width:width,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:10
    },
    clockHeader:{
        fontSize:25,
        color:'white',
        borderBottomWidth:2,
        borderBottomColor:'#d1da29',
        fontFamily:'Intro_Inline'
    },
    icon:{
        position:'absolute',
        right:50,
        
    },
    footerContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
        height: 70,
        backgroundColor: '#00cbff',
        marginTop:30,
        marginBottom: 25
    },
    footerText: {
        color: 'black',
        fontSize: 25,
        fontFamily: 'Intro_Inline'
    },

});
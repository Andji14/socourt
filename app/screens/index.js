import { MainScreen } from './MainScreen';
import { SelectWhatYouPlay } from './SelectWhatYouPlay';
import { SportType } from './SportType';
import { StartGame } from './StartGame';
import { GameInProgress } from './GameInProgress';

export {
    MainScreen,
    SelectWhatYouPlay,
    SportType,
    StartGame,
    GameInProgress
    };
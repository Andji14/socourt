import React, {Component} from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import Navigator from './config/routes';
import { Provider } from 'react-redux';
import store from './config/store';
// Redux Observable Parts

import {
  View,
  Text
} from 'react-native';

EStyleSheet.build({
  $background: '#00a2e2',
  $primaryBlue: '#093697',
});

export default () => (
  <Provider store={store}>
    <Navigator />
  </Provider>
);
